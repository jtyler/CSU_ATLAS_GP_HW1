#ifndef AnalysisCode_ZprimeAnalysis_H
#define AnalysisCode_ZprimeAnalysis_H

#include <EventLoop/Algorithm.h>
#include <TTree.h>
#include <TH1.h>
#include <TEfficiency.h>

#define GeV 0.001

class ZprimeAnalysis : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  TTree *tree; //!

  // Branches
  Int_t           runNumber;
  Int_t           eventNumber;
  Int_t           channelNumber;
  Float_t         mcWeight;
  Int_t           pvxp_n;
  Float_t         vxp_z;
  Float_t         scaleFactor_PILEUP;
  Float_t         scaleFactor_ELE;
  Float_t         scaleFactor_MUON;
  Float_t         scaleFactor_BTAG;
  Float_t         scaleFactor_TRIGGER;
  Float_t         scaleFactor_JVFSF;
  Float_t         scaleFactor_ZVERTEX;
  Bool_t          trigE;
  Bool_t          trigM;
  Bool_t          passGRL;
  Bool_t          hasGoodVertex;
  UInt_t          lep_n;
  Bool_t          lep_truthMatched[10];   //!
  UShort_t        lep_trigMatched[10];   //!
  Float_t         lep_pt[10];   //!
  Float_t         lep_eta[10];   //!
  Float_t         lep_phi[10];   //!
  Float_t         lep_E[10];   //!
  Float_t         lep_z0[10];   //!
  Float_t         lep_charge[10];   //!
  UInt_t          lep_type[10];   //!
  UInt_t          lep_flag[10];   //!
  Float_t         lep_ptcone30[10];   //!
  Float_t         lep_etcone20[10];   //!
  Float_t         lep_trackd0pvunbiased[10];   //!
  Float_t         lep_tracksigd0pvunbiased[10];   //!
  Float_t         met_et;
  Float_t         met_phi;
  UInt_t          jet_n;
  UInt_t          alljet_n;
  Float_t         jet_pt[30];   //!
  Float_t         jet_eta[30];   //!
  Float_t         jet_phi[30];   //!
  Float_t         jet_E[30];   //!
  Float_t         jet_m[30];   //!
  Float_t         jet_jvf[30];   //!
  Int_t           jet_trueflav[30];   //!
  Int_t           jet_truthMatched[30];   //!
  Float_t         jet_SV0[30];   //!
  Float_t         jet_MV1[30];   //!
  

  // histograms
  TH1F *h_lep_n; //!
  TH1F *h_lep_pt; //!
  TH1F *h_lep_eta; //!
  TH1F *h_lep_phi; //!
  TH1F *h_electron_n; //!
  TH1F *h_electron_pt; //!
  TH1F *h_electron_eta; //!
  TH1F *h_electron_phi; //!
  TH1F *h_muon_n; //!
  TH1F *h_muon_pt; //!
  TH1F *h_muon_eta; //!
  TH1F *h_muon_phi; //!

  TEfficiency *h_egamma_trig_eff; //!
  TEfficiency *h_muon_trig_eff; //!


  // this is a standard constructor
  ZprimeAnalysis ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(ZprimeAnalysis, 1);
};

#endif
